<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>closedsource-pom</artifactId>
        <version>5.0.26</version>
    </parent>

    <groupId>com.atlassian.confluence.plugins</groupId>
    <artifactId>confluence-layout-plugin</artifactId>
    <version>2.2.6-SNAPSHOT</version>
    <packaging>atlassian-plugin</packaging>

    <name>Layout Macros</name>
    <description>Macros to layout large blocks of information clearly in Confluence.</description>
    <url>https://ecosystem.atlassian.net/wiki/display/LAYOUT</url>
    <organization>
         <name>Atlassian</name>
         <url>http://www.atlassian.com/</url>
    </organization>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/confluence-layout-plugin.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/confluence-layout-plugin.git</developerConnection>
        <url>https://bitbucket.org/atlassian/confluence-layout-plugin</url>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <atlassian.plugin.key>confluence.extra.layout</atlassian.plugin.key>

        <confluence.version>6.6.15</confluence.version>
        <confluence.data.version>${confluence.version}</confluence.data.version>

        <atlassian.product.test-lib.version>2.13.3</atlassian.product.test-lib.version>

        <amps8.version>8.0.2</amps8.version>
        <containerId>tomcat8x</containerId>
        <webdriver.browser>firefox</webdriver.browser>

        <mockito.version>2.28.2</mockito.version>
        <byte-buddy.version>1.9.13</byte-buddy.version>

        <jvm.args.custom /> <!-- Allows to specify custom arguments in build scripts -->
        <jvm.args.xmx>1024m</jvm.args.xmx>
        <jvm.args>-Xmx${jvm.args.xmx} ${jvm.args.custom}</jvm.args>

        <!-- override the versions of the listed plugins in amps -->
        <version.override.set>maven-failsafe-plugin</version.override.set>
        <!-- failsafe 2.20.1 or newer required for rerun support. Double check the version from the parent-pom -->
        <surefire.version>2.22.0</surefire.version>
        <download.maven.plugin.version>1.3.0</download.maven.plugin.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.confluence</groupId>
                <artifactId>confluence-plugins-platform-pom</artifactId>
                <version>${confluence.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>${mockito.version}</version>
            </dependency>

            <!-- TODO remove version override when on Confluence 7.0.1 or higher -->
            <dependency>
                <groupId>net.bytebuddy</groupId>
                <artifactId>byte-buddy</artifactId>
                <version>${byte-buddy.version}</version>
            </dependency>
            <dependency>
                <groupId>net.bytebuddy</groupId>
                <artifactId>byte-buddy-agent</artifactId>
                <version>${byte-buddy.version}</version>
            </dependency>
            <dependency>
                <groupId>net.bytebuddy</groupId>
                <artifactId>byte-buddy-dep</artifactId>
                <version>${byte-buddy.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence</artifactId>
            <version>${confluence.version}</version>
            <scope>provided</scope>
            <exclusions>
                <exclusion>
                    <groupId>net.sourceforge.cssparser</groupId>
                    <artifactId>cssparser</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>net.sourceforge.nekohtml</groupId>
                    <artifactId>nekohtml</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence.plugin</groupId>
            <artifactId>func-test-package</artifactId>
            <version>${atlassian.product.test-lib.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-acceptance-test</artifactId>
            <version>${confluence.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>confluence-maven-plugin</artifactId>
                <version>${amps8.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <containerId>${containerId}</containerId>
                    <jvmArgs>${jvm.args}</jvmArgs>
                    <productVersion>${confluence.version}</productVersion>
                    <productDataVersion>${confluence.data.version}</productDataVersion>
                    <server>localhost</server>
                    <systemPropertyVariables>
                        <synchrony.proxy.enabled>false</synchrony.proxy.enabled>
                        <webdriver.browser>${webdriver.browser}</webdriver.browser>
                    </systemPropertyVariables>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
