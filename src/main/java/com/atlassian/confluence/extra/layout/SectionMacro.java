package com.atlassian.confluence.extra.layout;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SectionMacro extends BaseMacro
{
    private static final String MACRO_NAME = "section";

    /**
     * There are problems with the existing layout macro conversion so this is
     * turned off for LAYOUT-11
     */
    public boolean suppressSurroundingTagDuringWysiwygRendering()
    {
         return false;
    }

    public boolean suppressMacroRenderingDuringWysiwyg()
    {
        return true;
    }

    public String getName()
    {
        return MACRO_NAME;
    }

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        final String cssClass = Boolean.valueOf(StringUtils.defaultString((String) parameters.get("border"))).booleanValue()
                ? " class=\"sectionMacroWithBorder\""
                : " class=\"sectionMacro\"";

        // CONF-30239: Hack to display section macro correctly in the macro browser preview mode
        // This is necessary because extra empty paragraphs are inserted between the columns when the macro is being
        // previewed. This is not an issue on the view page.
        // the removeEmptyParagraphFromBody method removes a single empty paragraph (if there exists any) after each
        // <columnMacro>. This is because empty paragraphs are added between the columnMacros when they are rendered
        // in the editor.
        if (renderContext.getOutputType().equals("preview"))
        {
            Pattern emptyParagraphPattern = Pattern.compile("</div><p>(&nbsp;|&#160;|\\s|\\u00a0|)</p>");
            body = emptyParagraphPattern.matcher(body).replaceAll("</div>");
        }

        // The first case here is to handle any content inside the section macro that appears before the first
        // column macro. See LAYOUT-21. See CONF-43118 for why we do not use a regex here.
        int firstDivIndex = body.indexOf("<div");
        if (firstDivIndex > -1)
        {
            return new StringBuffer("<div class=\"sectionColumnWrapper\"><div").append(cssClass).append(">")
                    .append(body.substring(0, firstDivIndex))
                    .append("<div class=\"sectionMacroRow\">")
                    .append(body.substring(firstDivIndex, body.length()))
                    .append("</div></div></div>")
                    .toString();
        }
        else
        {
            return new StringBuffer("<div").append(cssClass).append(">")
                    .append("<div class=\"sectionMacroRow\">")
                    .append(body)
                    .append("</div></div>")
                    .toString();
        }
    }
}
