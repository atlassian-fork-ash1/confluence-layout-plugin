package it.com.atlassian.confluence.extra.layout;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.content.ViewContentBean;

import static net.sourceforge.jwebunit.junit.JWebUnit.getElementAttributByXPath;
import static net.sourceforge.jwebunit.junit.JWebUnit.getElementTextByXPath;

public class LayoutMacroFuncTest extends AbstractLayoutAcceptanceTest
{
	public void testColumnPercentageWidth() throws InterruptedException {
		Page page = new Page(TEST_SPACE, "testColumnWidth",
						"{" + COLUMN_MACRO_NAME + ":width=30%}\n" +
						"Text1\n" +
						"{" + COLUMN_MACRO_NAME + "}");

		wikiMarkupRpc.createPage(page);

		ViewContentBean.viewPage(page);

		assertEquals("width:30%;min-width:30%;max-width:30%;", getElementAttributByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][1]", "style"));
		assertEquals("Text1", getElementTextByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][1]//p"));
	}
	
	public void testColumnPixelWidth() throws InterruptedException {
		Page page = new Page(TEST_SPACE, "testColumnWidth",
				"{" + COLUMN_MACRO_NAME + ":width=100px}\n" +
						"Text1\n" +
						"{" + COLUMN_MACRO_NAME + "}");

		wikiMarkupRpc.createPage(page);

		ViewContentBean.viewPage(page);
		
		assertEquals("width:100px;min-width:100px;max-width:100px;", getElementAttributByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][1]", "style"));
		assertEquals("Text1", getElementTextByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][1]//p"));
	}
	
	public void testMultipleColumnPixelWidth() throws InterruptedException {
		Page page = new Page(TEST_SPACE, "testColumnWidth",
						"{" + COLUMN_MACRO_NAME + ":width=100px}\n" +
						"Text1\n" +
						"{" + COLUMN_MACRO_NAME + "}\n" +
						"{" + COLUMN_MACRO_NAME + ":width=200px}\n" +
						"Text2\n" +
						"{" + COLUMN_MACRO_NAME + "}");

		wikiMarkupRpc.createPage(page);

		ViewContentBean.viewPage(page);

		assertEquals("width:100px;min-width:100px;max-width:100px;", getElementAttributByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][1]", "style"));
		assertEquals("Text1", getElementTextByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][1]//p"));
		assertEquals("width:200px;min-width:200px;max-width:200px;", getElementAttributByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][2]", "style"));
		assertEquals("Text2", getElementTextByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][2]//p"));
	}
	
	public void testColumnWithAnotherMacro() throws InterruptedException {
		Page page = new Page(TEST_SPACE, "testColumnWidth",
						"{" + COLUMN_MACRO_NAME + ":width=30%}\n" +
						"{toc}\n" +
						"{" + COLUMN_MACRO_NAME + "}\n" +
						"h3. Title");

		wikiMarkupRpc.createPage(page);

		ViewContentBean.viewPage(page);

		assertEquals("width:30%;min-width:30%;max-width:30%;", getElementAttributByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][1]", "style"));
        //TODO: Research failed due to ul selector: assertLinkPresentWithText(getElementTextByXPath("//div[@class='wiki-content']//div[contains(@class,'columnMacro')][1]//div//ul//li//a[1]"));
	}
	
	public void testSectionWithColumnPercentageWidth() throws InterruptedException {
		Page page = new Page(TEST_SPACE, "testColumnWidth",
						"{" + SECTION_MACRO_NAME + "}\n" +
						"{" + COLUMN_MACRO_NAME + ":width=30%}\n" +
						"Text1\n" +
						"{" + COLUMN_MACRO_NAME + "}\n" +
						"{" + COLUMN_MACRO_NAME + ":width=50%}\n" +
						"Text2\n" +
						"{" + COLUMN_MACRO_NAME + "}\n" +
						"{" + SECTION_MACRO_NAME + "}");

		wikiMarkupRpc.createPage(page);

		ViewContentBean.viewPage(page);
		
		assertEquals("sectionMacro", getElementAttributByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']", "class"));
		assertEquals("width:30%;min-width:30%;max-width:30%;", getElementAttributByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']//div[@class='sectionMacroRow'][1]//div[contains(@class,'columnMacro')][1]", "style"));
		assertEquals("Text1", getElementTextByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']//div[@class='sectionMacroRow'][1]//div[contains(@class,'columnMacro')][1]//p"));
		assertEquals("width:50%;min-width:50%;max-width:50%;", getElementAttributByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']//div[@class='sectionMacroRow'][1]//div[contains(@class,'columnMacro')][2]", "style"));
		assertEquals("Text2", getElementTextByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']//div[@class='sectionMacroRow'][1]//div[contains(@class,'columnMacro')][2]//p"));
	}
	
	public void testSectionWithAnotherMacroInColumnPixelWidth() throws InterruptedException {
		Page page = new Page(TEST_SPACE, "testColumnWidth",
						"{" + SECTION_MACRO_NAME + "}\n" +
						"{" + COLUMN_MACRO_NAME + ":width=100px}\n" +
						"{toc}" +
						"{" + COLUMN_MACRO_NAME + "}\n" +
						"{" + COLUMN_MACRO_NAME + ":width=200px}\n" +
						"{" + SECTION_MACRO_NAME + "}\n" +
						"h3. Tile");

		wikiMarkupRpc.createPage(page);

		ViewContentBean.viewPage(page);
		
		assertEquals("sectionMacro", getElementAttributByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']", "class"));
		assertEquals("width:100px;min-width:100px;max-width:100px;", getElementAttributByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']//div[@class='sectionMacroRow'][1]//div[contains(@class,'columnMacro')][1]", "style"));
        //TODO: Research failed due to ul selector: assertLinkPresentWithText(getElementTextByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']//div[@class='sectionMacroRow'][1]//div[contains(@class,'columnMacro')][1]//div//ul//li//a[1]"));
		assertEquals("width:200px;min-width:200px;max-width:200px;", getElementAttributByXPath("//div[@class='wiki-content']//div[@class='sectionMacro']//div[@class='sectionMacroRow'][1]//div[contains(@class,'columnMacro')][2]", "style"));
	}
}
